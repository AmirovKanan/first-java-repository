package Warmup;

import java.util.Random;

public class Odd_or_Even {
    public static void main(String[] args) {
        int[] arr = new int[30];
        Random random = new Random();
        int max = 100;
        int min = -100;
        for (int i = 0; i < arr.length; i++) {
            int randNum = random.nextInt(max - min) + min;
            System.out.print(randNum + " ");
            arr[i] = randNum;
        }
        System.out.println();
        System.out.print("positive even: ");
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0 && arr[i] > 0) {
                System.out.print(arr[i] + " ");
            }
        }
        System.out.println();
        System.out.print("positive odd: ");
        for (int j = 0; j < arr.length; j++) {
            if (arr[j] % 2 == 1 && arr[j] > 0) {
                System.out.print(arr[j] + " ");
            }
        }
        System.out.println();
        System.out.print("negative even: ");
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0 && arr[i] < 0) {
                System.out.print(arr[i] + " ");
            }
        }
        System.out.println();
        System.out.print("negative odd: ");
        for (int k = 0; k < arr.length; k++) {
            if (arr[k] % 2 == -1 && arr[k] < 0) {
                System.out.print(arr[k] + " ");
            }
        }
    }
}


