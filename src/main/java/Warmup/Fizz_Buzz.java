package Warmup;

public class Fizz_Buzz {
    public static void main(String[] args) {
        for(int i=1; i<=30; i++) {
            if(i%2==0 && i%3==0){
            System.out.println(i + " Fizz-Buzz");
        }
            else if(i%2==0) {
                System.out.println(i + " Fizz");
            } else if(i%3==0){
                System.out.println(i + " Buzz");
            } else System.out.println(i+ " Nothing");

        }
    }
}
